// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

const ConvertToString = (arr) => {
  //firstly we are checking if array length is equal to 0 means we cannot make a string
  if (arr.length == 0) {
    //so we will return an empty string
    return "";
  } else {
    let result = "";

    //here we are loping over an array
    for (let index = 0; index < arr.length; index++) {
      //so here we are performing concat operation for each element in array to make it part of our result array
      result += arr[index];
      if (index == arr.length - 1) {
        break;
      }
      //it used to make an space between each element of an array
      result += " ";
    }

    result += ".";

    //here we are return our result string
    return result;
  }
};

module.exports = ConvertToString;
