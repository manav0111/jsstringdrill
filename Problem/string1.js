// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0.

//so this is a function used to convert a string into a Number
const convertStringToNumber = (str) => {
  // firstly we will use slice method of string it will slice the string from index 1
  //for eg if our str is this "$-1,002.22" so it will return the substr from index 1 and remove $ from our string "-1,002.22"
  const substr = str.slice(1);
  console.log(substr);

  //now here we can see the ',' in our substr so we will remove this ',' using repalce method
  const newstr = substr.replace(",", "");
  console.log(newstr);
  // so now or our newstr is this for eg-"-1,002.22" from this to this "-1002.22"

  console.log(typeof newstr);
  // but here we can see out newstr is of type is string

  // so to covert it to number qwe will use Number Method
  const num = Number(newstr);
  console.log(num);
  console.log(typeof num);
  // now it will convert it to the type number

  return num;
};

module.exports = convertStringToNumber;
