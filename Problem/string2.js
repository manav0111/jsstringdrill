// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

//This is the function used for the Split the String into array
const SplitArray = (str) => {
  //so we have given a string so to split a string we will used split method

  const arr = str.split(".");
  //to split we have provided the '.' method so that it will split  this according to '.'
  console.log(arr);
  //now here we have seen that it will split in string only

  //so here we will loop through our arr till it's length
  for (let index = 0; index < arr.length; index++) {
    //so here we are converting each array element into number
    arr[index] = Number(arr[index]);
  }

  return arr;
};

module.exports = SplitArray;
