// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const TitleCase = (obj) => {
  //firstly we will get the firstname,middlename,lastname from object
  let first_name = obj.first_name;
  let middle_name = obj.middle_name;
  let last_name = obj.last_name;

  //now here we will make First Character uppercase which will be present at 0 index
  //and after that we will get remaing string by using slice method and make the  whole string lowercase using to lowercase method
  first_name =
    first_name.charAt(0).toUpperCase() + first_name.slice(1).toLowerCase();
  middle_name =
    middle_name.charAt(0).toUpperCase() + middle_name.slice(1).toLowerCase();
  last_name =
    last_name.charAt(0).toUpperCase() + last_name.slice(1).toLowerCase();

  console.log(first_name, middle_name, last_name);

  //here we will concat our string to get the full name in one string
  const result = first_name + " " + middle_name + " " + last_name;
  console.log(result);

  //here we are returing our TitleCase string
  return result;
};

module.exports = TitleCase;
