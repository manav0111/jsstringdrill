// ==== String Problem #3 ====
// Given a string in the format of "20/1/2021", print the month in which the date is present in.

const MonthByDate = (str) => {
  //firsly we can split this using '/'

  const arr = str.split("/");
  console.log(arr);

  const Month = Number(arr[1]);
  //so in our arr at the first index we will get month and converted in into Number

  const Months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  //here we will create the months array to get the respective month

  const result = Months[Month - 1];
  //we are doing -1 because the array index will start from 1

  return result;
};

module.exports = MonthByDate;
