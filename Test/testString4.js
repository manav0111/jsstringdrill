const TitleCase = require("../Problem/string4");

const Name = TitleCase({
  first_name: "JoHN",
  middle_name: "doe",
  last_name: "SMith",
});

console.log(`TitleCase Name is : ${Name}}`);
